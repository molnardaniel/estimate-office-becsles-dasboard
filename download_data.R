secret_path <-  "~/google_auth" 
secret_file <- "estimate-214011-b726bc5764f3.json"
secret_file_with_path <- "~/google_auth/estimate-214011-b726bc5764f3.json"
if (!(file.exists(secret_file_with_path))) {
  dir.create(secret_path, showWarnings = FALSE)
  system(command = paste0("gsutil cp gs://estimate_temp_files/auth/",secret_file, " ", secret_path), wait = T)
  system(command = paste0('export GOOGLE_APPLICATION_CREDENTIALS="', secret_file_with_path, '"'))
}
print("secret created")

Sys.setenv("GCE_AUTH_FILE" = secret_file_with_path)
Sys.setenv("GCS_AUTH_FILE" = secret_file_with_path)
Sys.setenv("BQ_AUTH_FILE"  = secret_file_with_path)
Sys.setenv("GOOGLE_APPLICATION_CREDENTIALS"  = secret_file_with_path)
if (!"bigQueryR" %in% rownames(installed.packages())) {
  install.packages("bigQueryR")
}
library(bigQueryR)
print("bigqueryr installed and loaded")

bqr_auth()
print("bqr authorization successful")

bqr_global_project("estimate-214011")
bqr_global_dataset("estimate_API_stats")

my_query_office_becslesek <- "SELECT * FROM `estimate-214011.estimate_API_stats.eles_logs_office_all`"
api_becslesek_final <- bqr_query(query = my_query_office_becslesek, useLegacySql = FALSE)

my_query_estimate_funnel <- "SELECT * FROM `estimate-214011.estimate_API_stats.estimate_rolling_funnel`"
estimate_funnel <- bqr_query(query = my_query_estimate_funnel, useLegacySql = FALSE)

my_query_visszajelzes <- "SELECT * FROM `estimate-214011.estimate_API_stats.eles_visszajelzes`"
visszajelzes <- bqr_query(query = my_query_visszajelzes, useLegacySql = FALSE)
print("tablak letoltve")
if (!"data.table" %in% rownames(installed.packages())) {
  install.packages("data.table")
}
library(data.table)
fwrite(api_becslesek_final, "/home/molnar_daniel_ingatlan_com/office_becslesek_db/data/api_becslesek_final.csv")
#fwrite(api_becslesek_final, "/home/rstudio/Projects/office_becslesek/office_becsles_db/data/api_becslesek_final.csv")
fwrite(estimate_funnel, "/home/molnar_daniel_ingatlan_com/office_becslesek_db/data/estimate_rolling_funnel.csv")
#fwrite(estimate_funnel, "/home/rstudio/Projects/office_becslesek/office_becsles_db/data/estimate_rolling_funnel.csv")
fwrite(visszajelzes, "/home/molnar_daniel_ingatlan_com/office_becslesek_db/data/visszajelzes.csv")
#fwrite(visszajelzes, "/home/rstudio/Projects/office_becslesek/office_becsles_db/data/visszajelzes.csv")

print("tabla elmentve")
print("package installed")


response <-
  system(
    command = paste0("gsutil -q cp ", "gs://estimate_modelstore/zona_tiltolista/bizalmi_index_Bp.csv"," ", "data/bizalmi_index_Bp.csv"),
    wait = T,
    intern = T
  )


response <-
  system(
    command = paste0("gsutil -q cp ", "gs://estimate_modelstore/zona_tiltolista/bizalmi_index_Videk.csv"," ", "data/bizalmi_index_Videk.csv"),
    wait = T,
    intern = T
  )
